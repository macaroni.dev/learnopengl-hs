{-# LANGUAGE LambdaCase #-}
module Main where

import qualified LearnOpenGL.HelloWindow
import qualified LearnOpenGL.HelloTriangle
import qualified LearnOpenGL.Shaders
import qualified LearnOpenGL.Textures
import qualified LearnOpenGL.Transformations
import qualified LearnOpenGL.CoordinateSystems
import qualified LearnOpenGL.Camera
import System.Environment

commands :: [(String, IO ())]
commands =
  [ ("hello-window", LearnOpenGL.HelloWindow.main)
  , ("hello-triangle",  LearnOpenGL.HelloTriangle.main)
  , ("shaders", LearnOpenGL.Shaders.main)
  , ("textures", LearnOpenGL.Textures.main)
  , ("transformations", LearnOpenGL.Transformations.main)
  , ("coordinate-systems", LearnOpenGL.CoordinateSystems.main)
  , ("camera", LearnOpenGL.Camera.main)
  ]


main :: IO ()
main = getArgs >>= \case
  [] -> do
    putStrLn "Please provide a command:"
    putStrLn ""
    putStrLn $ unlines $ fmap fst commands
  cmd : _ -> case lookup cmd commands of
    Just prg -> prg
    Nothing -> do
      putStrLn $ "Unknown command: " ++ cmd
      putStrLn ""
      putStrLn "Valid commands:"
      putStrLn $ unlines $ fmap fst commands
