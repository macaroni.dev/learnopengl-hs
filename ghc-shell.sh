#! /usr/bin/env sh

nix-shell -p "haskellPackages.ghcWithPackages (pkgs: with pkgs; [ gl JuicyPixels GLFW-b raw-strings-qq vector linear containers ])"
