#version 330 core

out vec4 FragColor;
  
in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D ourTexture1;
uniform sampler2D ourTexture2;

void main() {
  //FragColor = texture(ourTexture, TexCoord);
  //FragColor = vec4(ourColor, 1.0);
  //FragColor = texture(ourTexture, TexCoord) * vec4(ourColor, 1.0);
  // Linearly interpolate between both textures (second texture is only slightly combined)
  FragColor = mix(texture(ourTexture1, TexCoord), texture(ourTexture2, TexCoord), 0.2) * vec4(ourColor, 1.0);
  //FragColor = texture(ourTexture1, TexCoord);
  //FragColor = texture(ourTexture2, TexCoord);

  // Shade red if transparent, green otherwise:
  /*
  vec4 color = texture(ourTexture2, TexCoord);
  if (color.w < 1) FragColor = vec4(1,0,0,1); else FragColor = vec4(0,1,0,1);
  */
}
