module LearnOpenGL.HelloWindow where

import Control.Monad (when)
import Control.Exception (bracket)
import qualified Graphics.UI.GLFW as GLFW
import Graphics.GL.Core33
import Control.Concurrent.STM
import Data.StateVar
import Debug.Trace

winWidth :: Int
winWidth = 800

winHeight :: Int
winHeight = 600

winTitle :: String
winTitle = "Hello Window"

-- | Ensures that we only run GLFW code while it's initialized, and also that we
-- always terminate it when we're done. Also, this function should only be used
-- from the main thread.
bracketGLFW :: IO () -> IO ()
bracketGLFW act = bracket GLFW.init (const GLFW.terminate) $ \initWorked ->
    when initWorked act

-- type KeyCallback = Window -> Key -> Int -> KeyState -> ModifierKeys -> IO ()
quitOnEscape :: GLFW.KeyCallback
quitOnEscape window key _scanCode keyState _modKeys = do
    print key
    when (key == GLFW.Key'Escape && keyState == GLFW.KeyState'Pressed)
        (GLFW.setWindowShouldClose window True)

type StatefulCallback s =
  forall v
   . HasGetter v s
  => HasSetter v s
  => HasUpdate v s s
  => v
  -> GLFW.KeyCallback

mainLoop
  :: forall s i
   . s
  -> StatefulCallback s
  -> (i -> s -> IO ())
  -> IO i -- ^ setup - last for hanging "do"
  -> IO ()
mainLoop s'init kcb draw setup = bracketGLFW $ do
    tvar <- newTVarIO s'init
    GLFW.windowHint (GLFW.WindowHint'ContextVersionMajor 3)
    GLFW.windowHint (GLFW.WindowHint'ContextVersionMinor 3)
    GLFW.windowHint (GLFW.WindowHint'OpenGLProfile GLFW.OpenGLProfile'Core)
    GLFW.windowHint (GLFW.WindowHint'Resizable False)
    maybeWindow <- GLFW.createWindow winWidth winHeight winTitle Nothing Nothing
    case maybeWindow of
        Nothing -> putStrLn "Failed to create a GLFW window!"
        Just window -> do
            -- enable keys
            GLFW.setKeyCallback window (Just $ kcb tvar)
            -- calibrate the viewport
            GLFW.makeContextCurrent (Just window)
            (x,y) <- GLFW.getFramebufferSize window
            glViewport 0 0 (fromIntegral x) (fromIntegral y)

            i <- setup
            -- enter our main loop
            let loop = do
                    shouldContinue <- not <$> GLFW.windowShouldClose window
                    when shouldContinue $ do
                        -- event poll
                        GLFW.pollEvents
                        -- drawing
                        glClearColor 0.2 0.3 0.3 1.0
                        glClear GL_COLOR_BUFFER_BIT
                        readTVarIO tvar >>= draw i
                        -- swap buffers and go again
                        GLFW.swapBuffers window
                        loop
            loop

main ::  IO ()
main = mainLoop () (\_ -> quitOnEscape) (\() () -> pure ()) (pure ())
