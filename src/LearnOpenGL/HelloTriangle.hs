{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NegativeLiterals #-}

module LearnOpenGL.HelloTriangle where

import Control.Monad (when)
import Data.StateVar
import Data.Foldable (for_, toList)
import Data.Traversable (for)
import Foreign
import Foreign.C.String (withCAStringLen)
import qualified Graphics.UI.GLFW as GLFW
import Graphics.GL.Core33
import Graphics.GL.Types
import Graphics.GL.Simple
import Text.RawString.QQ
import Data.Functor.Identity
import Linear.V1
import Linear.V3
import Linear.V
import GHC.TypeLits
import Data.Proxy
import Data.Tuple (Solo (..))

import LearnOpenGL.HelloWindow (StatefulCallback)
import qualified LearnOpenGL.HelloWindow


import Debug.Trace


shaderSrc :: ShaderSrc
shaderSrc = ShaderSrc
  { vertexSrc = [r|#version 330 core
    layout (location = 0) in vec3 position;
    void main()
    {
        gl_Position = vec4(position.x, position.y, position.z, 1.0);
    }
    |]
  , fragmentSrc = [r|#version 330 core
    out vec4 color;
    void main()
    {
        color = vec4(1.0f, 0.5f, 0.2f, 1.0f);
    }
    |]
 }

data HelloTriangleInit = HelloTriangleInit
  { helloTriVAO :: GLuint
  , helloRectVAO :: GLuint
  , helloShader :: GLuint
  }

data HelloTriangleVerts a = HelloTriangleVerts
  { triVerts :: a
  , rectVerts :: a
  } deriving stock (Functor, Foldable, Traversable)

draw :: HelloTriangleInit -> HelloTriangleState -> IO ()
draw HelloTriangleInit{..} HelloTriangleState{..} = do
  case drawShape of
    HelloTri -> do
      glUseProgram helloShader
      glBindVertexArray helloTriVAO
      glDrawArrays GL_TRIANGLES 0 3
    HelloRect -> do
      glUseProgram helloShader
      glBindVertexArray helloRectVAO
      glDrawElements GL_TRIANGLES 6 GL_UNSIGNED_INT nullPtr

  let polyMode = if wireframeMode then GL_LINE else GL_FILL
  glPolygonMode GL_FRONT_AND_BACK polyMode
  glBindVertexArray 0

triVertices :: VertConfig [] V1 V3
triVertices = VertConfig
  { vertices =
      fmap V1
      [ V3 -0.5 -0.5 0.0
      , V3 0.5 -0.5 0.0
      , V3 0.0 0.5 0.0
      ]
  , mIndices = Nothing
  }

triVertices2 :: [Solo (V3 GLfloat)]
triVertices2 = fmap Solo
  [ V3 -0.5 -0.5 0.0
  , V3 0.5 -0.5 0.0
  , V3 0.0 0.5 0.0
  ]

rectVertices2 :: [Solo (V3 GLfloat)]
rectVertices2 = fmap Solo
  [ -- first triangle
    V3 0.5  0.5 0.0  -- top right
  , V3 0.5 -0.5 0.0  -- bottom right
  , V3 -0.5  0.5 0.0  -- top left

    -- second triangle
  , V3 0.5 -0.5 0.0  -- bottom right
  , V3 -0.5 -0.5 0.0  -- bottom left
  , V3 -0.5  0.5 0.0   -- top left
  ]

rectVertices :: VertConfig [] V1 V3
rectVertices = VertConfig
  { vertices =
    fmap V1
    [ V3 0.5 0.5 0.0  -- Top Right
    , V3 0.5 -0.5 0.0  -- Bottom Right
    , V3 -0.5 -0.5 0.0 -- Bottom Left
    , V3 -0.5 0.5 0.0  -- Top Left
    ]
  , mIndices =
    Just
    [  -- Note that we start from 0!
      0, 1, 3, -- First Triangle
      1, 2, 3  -- Second Triangle
    ]
  }

data SetupVerts vf shf = SetupVerts
  { setupVAOs :: vf GLuint
  , setupProgs :: shf GLuint
  }

setupVAO :: SetupVerts Identity a -> GLuint
setupVAO = runIdentity . setupVAOs

setupProg :: SetupVerts a Identity -> GLuint
setupProg = runIdentity . setupProgs

data VertConfig f d v = VertConfig
  { vertices :: f (d (v GLfloat))
  , mIndices :: Maybe (f GLuint)
  }

setup
  :: forall shf vf f d v
   . Traversable shf
  => Traversable vf
  => Foldable f
  => Finite d
  => Finite v
  => KnownNat (Size d)
  => KnownNat (Size v)
  => shf ShaderSrc
  -> vf (VertConfig f d v)
  -> IO (SetupVerts vf shf)
setup shaders vertCfgs = do
  -- ready our program(s)
  setupProgs <- for shaders setupShader

  -- setup our vertices
  setupVAOs <- for vertCfgs $ \VertConfig{..} -> do
    let verticesSize = fromIntegral $ sizeOf (0.0 :: GLfloat) * (length vertices) * finiteLength @d * finiteLength @v
    let flatVerts :: [GLfloat] = do
          dvs <- toList vertices
          vs <- toList $ toVector $ toV dvs
          toList $ toVector $ toV vs
    traceIO $ unwords ["flatVerts =", show flatVerts]
    verticesP <- newArray flatVerts

     -- setup a vertex array object
    vaoP <- malloc
    glGenVertexArrays 1 vaoP
    vao <- peek vaoP
    glBindVertexArray vao
      -- unbind our vertex array & attrib objects to prevent accidental changes in
      -- between our draw calls.
      --glEnableVertexAttribArray 0
    -- setup a vertex buffer object and send it data
    vboP <- malloc
    glGenBuffers 1 vboP
    vbo <- peek vboP
    glBindBuffer GL_ARRAY_BUFFER vbo
    glBufferData GL_ARRAY_BUFFER verticesSize (castPtr verticesP) GL_STATIC_DRAW
    --glBindVertexArray 0

    -- setup an element buffer object and send it data
    for_ mIndices $ \indices -> do
      let indicesSize = fromIntegral $ sizeOf (0 :: GLuint) * (length indices)
      indicesP <- newArray (toList indices)
      eboP <- malloc
      glGenBuffers 1 eboP
      ebo <- peek eboP
      glBindBuffer GL_ELEMENT_ARRAY_BUFFER ebo
      glBufferData GL_ELEMENT_ARRAY_BUFFER indicesSize (castPtr indicesP) GL_STATIC_DRAW

    -- assign the attribute pointer information
    for_ [0..finiteLength @d - 1] $ \i -> do
      let floatSize = sizeOf (0.0::GLfloat)
      let nv = finiteLength @v
      let nd = finiteLength @d
      let nvFloats = fromIntegral $ floatSize * nv * nd
      let offset = i * nv * floatSize
      -- TODO: Configurable logger
      traceIO $ unwords
        [ "nvFloats =", show nvFloats
        , "i =", show i
        , "len v =", show nv
        , "offset =", show offset
        ]

      glVertexAttribPointer (fromIntegral i) (fromIntegral nv) GL_FLOAT GL_FALSE nvFloats (nullPtr `plusPtr` offset)
      glEnableVertexAttribArray (fromIntegral i)

    glBindVertexArray 0

    pure vao

  pure SetupVerts {..}

data HelloTriangleShape =
    HelloTri
  | HelloRect

data HelloTriangleState = HelloTriangleState
  { wireframeMode :: Bool
  , drawShape :: HelloTriangleShape
  }

defaultState :: HelloTriangleState
defaultState = HelloTriangleState
  { wireframeMode = False
  , drawShape = HelloTri
  }

callback :: StatefulCallback HelloTriangleState
callback var window key scanCode keyState modKeys = do
  LearnOpenGL.HelloWindow.quitOnEscape window key scanCode keyState modKeys

  when (key == GLFW.Key'W && keyState == GLFW.KeyState'Pressed) $
    var $~ (\s -> s { wireframeMode = not $ wireframeMode s })

  when (key == GLFW.Key'R && keyState == GLFW.KeyState'Pressed) $
    var $~ (\s -> s { drawShape = HelloRect })

  when (key == GLFW.Key'T && keyState == GLFW.KeyState'Pressed) $
    var $~ (\s -> s { drawShape = HelloTri })

main :: IO ()
main =
  LearnOpenGL.HelloWindow.mainLoop
  defaultState
  callback
  draw $ do
    helloShader <- setupShader shaderSrc
    helloTriVAO <- glSetupRecordArrayBuffer triVertices2 Nothing
    helloRectVAO <- glSetupRecordElementBuffer rectVertices2
    
    pure HelloTriangleInit{..}

finiteLength :: forall v. Finite v => KnownNat (Size v) => Int
finiteLength = fromIntegral $ natVal $ Proxy @(Size v)
