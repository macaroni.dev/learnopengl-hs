{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}

module LearnOpenGL.Shaders where

import Control.Monad (when)
import Foreign.C.String (withCString)
import qualified Graphics.UI.GLFW as GLFW
import Graphics.GL.Core33
import Graphics.GL.Types
import Graphics.GL.Simple
import Data.StateVar
import Linear
import Data.Tuple (Solo (..))
import GHC.Generics

import LearnOpenGL.HelloWindow (mainLoop, quitOnEscape, StatefulCallback)
import LearnOpenGL.HelloTriangle (triVertices2)

data Shaders a = Shaders
  { colorUniform :: a
  , colorVertexShader :: a
  , colorVertexData :: a
  } deriving stock (Functor, Foldable, Traversable)

shaderSrcs :: Shaders ShaderSrc
shaderSrcs = Shaders
  { colorUniform = $(embedModuleShaderSrc (Just "color-uniform"))
  , colorVertexShader = $(embedModuleShaderSrc (Just "color-vertex-shader"))
  , colorVertexData = $(embedModuleShaderSrc (Just "color-vertex-data"))
  }

data ShaderState = ColorVertexShader | ColorUniform | ColorVertexData
  deriving stock (Eq, Enum, Bounded)

callback :: StatefulCallback ShaderState
callback var window key scanCode keyState modKeys = do
  quitOnEscape window key scanCode keyState modKeys
  when (key == GLFW.Key'Space && keyState == GLFW.KeyState'Pressed) $
    var $~ \s -> if s == maxBound then minBound else succ s

data PositionColor = PositionColor
  { pcPosition :: V3 GLfloat
  , pcColor :: V3 GLfloat
  } deriving stock (Eq, Ord, Show, Generic)

triColorVertices :: [PositionColor]
triColorVertices =
  zipWith (\(Solo p) c -> PositionColor p c)
    triVertices2
    [V3 1.0 0.0 0.0, V3 0.0 1.0 0.0, V3 0.0 0.0 1.0]

data ShaderInit = ShaderInit
  { triVAO :: GLuint
  , triColorVAO :: GLuint
  , shaderProgs :: Shaders GLuint
  }

draw :: ShaderInit -> ShaderState -> IO ()
draw ShaderInit{..} ss = do
  t :: GLfloat <- GLFW.getTime >>= \case
    Nothing -> error "GLFW.getTime failed!"
    Just x -> pure $ realToFrac x
  case ss of
    ColorVertexShader -> do
      let sh = colorVertexShader shaderProgs
      glUseProgram sh
      glBindVertexArray triVAO
      glDrawArrays GL_TRIANGLES 0 3
    ColorUniform -> do
      let sh = colorUniform shaderProgs
      let greenValue = sin(t) / 2.0 + 0.5
      glUseProgram sh
      loc <- withCString "ourColor" $ glGetUniformLocation sh
      glUniform4f loc 0.0 greenValue 0.0 1.0
      glBindVertexArray triVAO
      glDrawArrays GL_TRIANGLES 0 3
    ColorVertexData -> do
      let sh = colorVertexData shaderProgs
      glUseProgram sh
      glBindVertexArray triColorVAO
      glDrawArrays GL_TRIANGLES 0 3


main :: IO ()
main =
  mainLoop
  ColorVertexShader
  callback
  draw $ do
    shaderProgs <- traverse setupShader shaderSrcs
    triVAO <- glSetupRecordArrayBuffer triVertices2 Nothing
    triColorVAO <- glSetupRecordArrayBuffer triColorVertices Nothing
    pure ShaderInit{..}
