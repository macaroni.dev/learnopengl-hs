{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NegativeLiterals #-}

module LearnOpenGL.CoordinateSystems where

import Data.Foldable
import Control.Monad
import Linear
import Data.StateVar
import Graphics.GL.Types
import Graphics.GL.Simple
import Graphics.GL.Core33
import Foreign
import Foreign.C.String
import qualified Graphics.UI.GLFW as GLFW

import LearnOpenGL.HelloWindow (mainLoop, quitOnEscape, StatefulCallback)
import GHC.Generics

callback :: StatefulCallback CoordState
callback var window key scanCode keyState modKeys = do
  quitOnEscape window key scanCode keyState modKeys
  when (key == GLFW.Key'Space && keyState == GLFW.KeyState'Pressed) $
    var $~ \s -> if s == maxBound then minBound else succ s

data CoordInit = CoordInit
  { tex0 :: GLuint
  , tex1 :: GLuint
  , cubeVAO :: GLuint
  , texShader :: GLuint
  }

texShaderSrc :: ShaderSrc
texShaderSrc = $(embedModuleShaderSrc Nothing)
--
data CoordState =
    CoordPlane
  | CoordCubes
  deriving stock (Show, Eq, Enum, Bounded)

draw :: CoordInit -> CoordState -> IO ()
draw CoordInit{..} _ = do
  glClear (GL_COLOR_BUFFER_BIT .|. GL_DEPTH_BUFFER_BIT)
  glEnable GL_DEPTH_TEST
  
  glUseProgram texShader

  t :: GLfloat <- GLFW.getTime >>= \case
    Nothing -> error "GLFW.getTime failed!"
    Just x -> pure $ realToFrac x

  let screenWidthF = 800
  let screenHeightF = 600

  let viewMat = mkTransformation (axisAngle (V3 (0::GLfloat) 0 1) 0) (V3 0 0 (-3))
  let projMat = perspective 45 (screenWidthF / screenHeightF) 0.1 100.0

  glSetUniformMatrix4fv texShader "view" True viewMat
  glSetUniformMatrix4fv texShader "projection" True projMat

  glActiveTexture GL_TEXTURE0
  glBindTexture GL_TEXTURE_2D tex0
  loc0 <- withCString "ourTexture1" $ glGetUniformLocation texShader
  glUniform1i loc0 0

  glActiveTexture GL_TEXTURE1
  glBindTexture GL_TEXTURE_2D tex1
  loc1 <- withCString "ourTexture2" $ glGetUniformLocation texShader
  glUniform1i loc1 1

  glBindVertexArray cubeVAO
  for_ (cubes `zip` [0..]) $ \(cube, i) -> do
    let a = i*20
    let modelMat = mkTransformation (axisAngle (V3 (0.5::GLfloat) 1 0) t) cube
    glSetUniformMatrix4fv texShader "model" True modelMat
    glDrawArrays GL_TRIANGLES 0 36

  pure ()

data PosTex = PosTex
  { pctPosition :: V3 GLfloat
  , pctTexCoords :: V2 GLfloat
  } deriving stock (Eq, Ord, Show, Generic)

cubeVerts :: [PosTex]
cubeVerts =
  [ PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 0.0)
  , PosTex (V3  0.5 -0.5 -0.5)  (V2 1.0 0.0)
  , PosTex (V3  0.5  0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3  0.5  0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3 -0.5  0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 0.0)

  , PosTex (V3 -0.5 -0.5  0.5)  (V2 0.0 0.0)
  , PosTex (V3  0.5 -0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3  0.5  0.5  0.5)  (V2 1.0 1.0)
  , PosTex (V3  0.5  0.5  0.5)  (V2 1.0 1.0)
  , PosTex (V3 -0.5  0.5  0.5)  (V2 0.0 1.0)
  , PosTex (V3 -0.5 -0.5  0.5)  (V2 0.0 0.0)
  
  , PosTex (V3 -0.5  0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3 -0.5  0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3 -0.5 -0.5  0.5)  (V2 0.0 0.0)
  , PosTex (V3 -0.5  0.5  0.5)  (V2 1.0 0.0)
  
  , PosTex (V3 0.5  0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3 0.5  0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3 0.5 -0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3 0.5 -0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3 0.5 -0.5  0.5)  (V2 0.0 0.0)
  , PosTex (V3 0.5  0.5  0.5)  (V2 1.0 0.0)
  
  , PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3  0.5 -0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3  0.5 -0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3  0.5 -0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3 -0.5 -0.5  0.5)  (V2 0.0 0.0)
  , PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 1.0)
  
  , PosTex (V3 -0.5  0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3  0.5  0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3  0.5  0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3  0.5  0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3 -0.5  0.5  0.5)  (V2 0.0 0.0)
  , PosTex (V3 -0.5  0.5 -0.5)  (V2 0.0 1.0)
  ]

cubes :: [V3 GLfloat]
cubes = [
    V3 0 0 0,
    V3 2 5 (-15),
    V3 (-1.5) (-2.2) (-2.5),
    V3 (-3.8) (-2) (-12.3),
    V3 2.4 (-0.4) (-3.5),
    V3 (-1.7) 3 (-7.5),
    V3 1.3 (-2) (-2.5),
    V3 1.5 2 (-2.5),
    V3 1.5 0.2 (-1.5),
    V3 (-1.3) 1 (-1.5)
    ]

main :: IO ()
main =
  mainLoop
  CoordPlane
  callback
  draw $ do
    -- Set blending so transparent images render (e.g. if we render the awesomeface alone)
    glEnable (GL_BLEND .|. GL_DEPTH_TEST)
    glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    
    texShader <- setupShader texShaderSrc
    cubeVAO  <- glSetupRecordArrayBuffer cubeVerts Nothing
    tex0 <- glLoadPictureFile "./data/learnopengl/textures/wall.jpg"
    tex1 <- glLoadPictureFile "./data/learnopengl/textures/awesomeface.png"
    pure CoordInit{..}
