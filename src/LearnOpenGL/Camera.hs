{-# LANGUAGE NegativeLiterals #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE RecordWildCards #-}

module LearnOpenGL.Camera where

import Data.Foldable
import Control.Monad
import Linear
import Data.StateVar
import Graphics.GL.Types
import Graphics.GL.Simple
import Graphics.GL.Core33
import Foreign
import Foreign.C.String
import qualified Graphics.UI.GLFW as GLFW

import LearnOpenGL.HelloWindow (mainLoop, quitOnEscape, StatefulCallback)
import GHC.Generics

data Camera = Camera
  { cameraPos :: V3 GLfloat
  , cameraFront :: V3 GLfloat
  , cameraUp :: V3 GLfloat
  } deriving Show

initialCamera :: Camera
initialCamera = Camera (V3 0 0 3) (V3 0 0 (-1)) (V3 0 1 0)

toViewMatrix :: Camera -> M44 GLfloat
toViewMatrix Camera{..} = lookAt cameraPos (cameraPos ^+^ cameraFront) cameraUp

data MouseInfo = MouseInfo
  { lastXY :: Maybe (Double,Double)
  , oldPitchYaw :: (Double, Double)
  , frontVec :: V3 GLfloat
  } deriving Show

texShaderSrc :: ShaderSrc
texShaderSrc = $(embedModuleShaderSrc Nothing)

data PosTex = PosTex
  { pctPosition :: V3 GLfloat
  , pctTexCoords :: V2 GLfloat
  } deriving stock (Eq, Ord, Show, Generic)

cubeVerts :: [PosTex]
cubeVerts =
  [ PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 0.0)
  , PosTex (V3  0.5 -0.5 -0.5)  (V2 1.0 0.0)
  , PosTex (V3  0.5  0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3  0.5  0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3 -0.5  0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 0.0)

  , PosTex (V3 -0.5 -0.5  0.5)  (V2 0.0 0.0)
  , PosTex (V3  0.5 -0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3  0.5  0.5  0.5)  (V2 1.0 1.0)
  , PosTex (V3  0.5  0.5  0.5)  (V2 1.0 1.0)
  , PosTex (V3 -0.5  0.5  0.5)  (V2 0.0 1.0)
  , PosTex (V3 -0.5 -0.5  0.5)  (V2 0.0 0.0)
  
  , PosTex (V3 -0.5  0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3 -0.5  0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3 -0.5 -0.5  0.5)  (V2 0.0 0.0)
  , PosTex (V3 -0.5  0.5  0.5)  (V2 1.0 0.0)
  
  , PosTex (V3 0.5  0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3 0.5  0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3 0.5 -0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3 0.5 -0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3 0.5 -0.5  0.5)  (V2 0.0 0.0)
  , PosTex (V3 0.5  0.5  0.5)  (V2 1.0 0.0)
  
  , PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3  0.5 -0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3  0.5 -0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3  0.5 -0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3 -0.5 -0.5  0.5)  (V2 0.0 0.0)
  , PosTex (V3 -0.5 -0.5 -0.5)  (V2 0.0 1.0)
  
  , PosTex (V3 -0.5  0.5 -0.5)  (V2 0.0 1.0)
  , PosTex (V3  0.5  0.5 -0.5)  (V2 1.0 1.0)
  , PosTex (V3  0.5  0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3  0.5  0.5  0.5)  (V2 1.0 0.0)
  , PosTex (V3 -0.5  0.5  0.5)  (V2 0.0 0.0)
  , PosTex (V3 -0.5  0.5 -0.5)  (V2 0.0 1.0)
  ]

cubes :: [V3 GLfloat]
cubes = [
    V3 0 0 0,
    V3 2 5 (-15),
    V3 (-1.5) (-2.2) (-2.5),
    V3 (-3.8) (-2) (-12.3),
    V3 2.4 (-0.4) (-3.5),
    V3 (-1.7) 3 (-7.5),
    V3 1.3 (-2) (-2.5),
    V3 1.5 2 (-2.5),
    V3 1.5 0.2 (-1.5),
    V3 (-1.3) 1 (-1.5)
    ]

data CameraState = CameraState
  { camera :: Camera
  } deriving Show

callback :: StatefulCallback CameraState
callback var window key scanCode keyState modKeys = do
  quitOnEscape window key scanCode keyState modKeys

draw :: CameraInit -> CameraState -> IO ()
draw CameraInit{..} CameraState{..} = do
  glClear (GL_COLOR_BUFFER_BIT .|. GL_DEPTH_BUFFER_BIT)
  glEnable GL_DEPTH_TEST
  
  glUseProgram texShader

  t :: GLfloat <- GLFW.getTime >>= \case
    Nothing -> error "GLFW.getTime failed!"
    Just x -> pure $ realToFrac x

  let screenWidthF = 800
  let screenHeightF = 600

  --let viewMat = mkTransformation (axisAngle (V3 (0::GLfloat) 0 1) 0) (V3 0 0 (-3))
  let viewMat = toViewMatrix camera
  let projMat = perspective 45 (screenWidthF / screenHeightF) 0.1 100.0

  glSetUniformMatrix4fv texShader "view" True viewMat
  glSetUniformMatrix4fv texShader "projection" True projMat

  glActiveTexture GL_TEXTURE0
  glBindTexture GL_TEXTURE_2D tex0
  loc0 <- withCString "ourTexture1" $ glGetUniformLocation texShader
  glUniform1i loc0 0

  glActiveTexture GL_TEXTURE1
  glBindTexture GL_TEXTURE_2D tex1
  loc1 <- withCString "ourTexture2" $ glGetUniformLocation texShader
  glUniform1i loc1 1

  glBindVertexArray cubeVAO
  for_ (cubes `zip` [0..]) $ \(cube, i) -> do
    -- spin spin!
    let a = 20*(fromIntegral i) + if i `mod` 3 == 0 then t else 0
    let modelMat = mkTransformation (axisAngle (V3 (0.5::GLfloat) 1 0) a) cube
    glSetUniformMatrix4fv texShader "model" True modelMat
    glDrawArrays GL_TRIANGLES 0 36

  pure ()


data CameraInit = CameraInit
  { tex0 :: GLuint
  , tex1 :: GLuint
  , cubeVAO :: GLuint
  , texShader :: GLuint
  }

main :: IO ()
main =
  mainLoop
  CameraState { camera = initialCamera}
  callback
  draw $ do
    -- Set blending so transparent images render (e.g. if we render the awesomeface alone)
    glEnable (GL_BLEND .|. GL_DEPTH_TEST)
    glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    
    texShader <- setupShader texShaderSrc
    cubeVAO  <- glSetupRecordArrayBuffer cubeVerts Nothing
    tex0 <- glLoadPictureFile "./data/learnopengl/textures/wall.jpg"
    tex1 <- glLoadPictureFile "./data/learnopengl/textures/awesomeface.png"
    pure CameraInit{..}
