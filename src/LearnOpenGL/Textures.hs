{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NegativeLiterals #-}

module LearnOpenGL.Textures where

import Linear
import Graphics.GL.Types
import Graphics.GL.Simple
import Graphics.GL.Core33
import Foreign
import Foreign.C.String

import LearnOpenGL.HelloWindow (mainLoop, quitOnEscape, StatefulCallback)
import GHC.Generics

callback :: StatefulCallback TextureState
callback var window key scanCode keyState modKeys = do
  quitOnEscape window key scanCode keyState modKeys

data TextureInit = TextureInit
  { tex0 :: GLuint
  , tex1 :: GLuint
  , rectVAO :: GLuint
  , texShader :: GLuint
  }

texShaderSrc :: ShaderSrc
texShaderSrc = $(embedModuleShaderSrc Nothing)
--
data TextureState = TextureState

draw :: TextureInit -> TextureState -> IO ()
draw TextureInit{..} _ = do
  glEnable GL_BLEND
  
  glUseProgram texShader

  glActiveTexture GL_TEXTURE0
  glBindTexture GL_TEXTURE_2D tex0
  loc0 <- withCString "ourTexture1" $ glGetUniformLocation texShader
  glUniform1i loc0 0

  glActiveTexture GL_TEXTURE1
  glBindTexture GL_TEXTURE_2D tex1
  loc1 <- withCString "ourTexture2" $ glGetUniformLocation texShader
  glUniform1i loc1 1

  glBindVertexArray rectVAO
  glDrawElements GL_TRIANGLES 6 GL_UNSIGNED_INT nullPtr

data PosColorTex = PosColorTex
  { pctPosition :: V3 GLfloat
  , pctColor :: V3 GLfloat
  , pctTexCoords :: V2 GLfloat
  } deriving stock (Eq, Ord, Show, Generic)

rectTexVertices :: [PosColorTex]
rectTexVertices =
  [ -- first triangle:
    --  top right
    PosColorTex (V3 0.5 0.5 0.0) (V3 1.0 0.0 0.0) (V2 1.0 1.0)
    --  bottom right
  , PosColorTex (V3 0.5 -0.5 0.0) (V3 0.0 1.0 0.0) (V2 1.0 0.0)
    --  top left
  , PosColorTex (V3 -0.5 0.5 0.0) (V3 1.0 1.0 0.0) (V2 0.0 1.0)
    -- second triangle:
    --  bottom left
  , PosColorTex (V3 -0.5 -0.5 0.0) (V3 0.0 0.0 1.0) (V2 0.0 0.0)
    --  bottom right
  , PosColorTex (V3 0.5 -0.5 0.0) (V3 0.0 1.0 0.0) (V2 1.0 0.0)
    --  top left
  , PosColorTex (V3 -0.5 0.5 0.0) (V3 1.0 1.0 0.0) (V2 0.0 1.0)
  ]
-- ^ TODO: Drawing the rectangle is what crashes it. Probably something to do with either
-- element buffer or generic vertex attribs..need to test/debug

main :: IO ()
main =
  mainLoop
  TextureState
  callback
  draw $ do
    -- Set blending so transparent images render (e.g. if we render the awesomeface alone)
    glEnable GL_BLEND
    glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
    
    texShader <- setupShader texShaderSrc
    rectVAO  <- glSetupRecordElementBuffer rectTexVertices
    tex0 <- glLoadPictureFile "./data/learnopengl/textures/wall.jpg"
    tex1 <- glLoadPictureFile "./data/learnopengl/textures/awesomeface.png"
    pure TextureInit{..}
