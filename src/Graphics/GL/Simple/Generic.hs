{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DerivingStrategies #-}

module Graphics.GL.Simple.Generic where

import Data.Foldable (for_)
import Control.Monad.IO.Class
import Linear.V
import GHC.Generics
import Foreign.Ptr
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Storable
import Graphics.GL.Core33
import Graphics.GL.Types
import Data.Proxy
import GHC.TypeLits
import qualified Data.Set as Set

import Debug.Trace

glSetupRecordArrayBuffer
  :: forall a m
   . MonadIO m
  => Generic a
  => Show a
  => Ord a
  => GVertex (Rep a)
  => [a]
  -> Maybe [GLuint] -- ^ indices
  -> m GLuint -- ^ VAO
glSetupRecordArrayBuffer vs mIdxs = liftIO $ do
  alloca $ \vaoP -> do
    glGenVertexArrays 1 vaoP
    vao <- peek vaoP
    glBindVertexArray vao

    alloca $ \vboP -> do
      glGenBuffers 1 vboP
      vbo <- peek vboP
      glBindBuffer GL_ARRAY_BUFFER vbo

      glBufferRecord GL_ARRAY_BUFFER vs GL_STATIC_DRAW

      for_ mIdxs $ \idxs ->
        withArray idxs $ \idxsP -> do
          let idxSize = fromIntegral $ sizeOf (undefined :: GLuint) * (length idxs)
          alloca $ \eboP -> do
            glGenBuffers 1 eboP
            ebo <- peek eboP
            glBindBuffer GL_ELEMENT_ARRAY_BUFFER ebo
            glBufferData GL_ELEMENT_ARRAY_BUFFER idxSize (castPtr idxsP) GL_STATIC_DRAW

    glVertexAttribRecord @a
    --glEnableVertexAttribRecord @a

    pure vao

-- TESTME
glIndexVerts
  :: forall a
   . Ord a
  => [a]
  -> ([a], Maybe [GLuint])
glIndexVerts vs =
  let vset = Set.fromList vs
      getIdx v = case Set.lookupIndex v vset of
        Just i -> i
        Nothing -> error "glIndexVerts - Impossible! Did not find vertex in Set"
  in if length vs == Set.size vset
     then (vs, Nothing)
     else (Set.toAscList vset, Just $ fmap (fromIntegral . getIdx) vs)

glSetupRecordElementBuffer
  :: forall a m
   . MonadIO m
  => Generic a
  => Show a
  => Ord a
  => GVertex (Rep a)
  => [a]
  -> m GLuint -- ^ VAO
glSetupRecordElementBuffer = uncurry glSetupRecordArrayBuffer . glIndexVerts

data AttribSpec = AttribSpec
  { asSize :: GLuint
  , asType :: GLenum
  , asNormalized :: Bool
  , asByteSize :: GLsizei
  } deriving stock (Show)

glBufferRecord
  :: forall a m
   . MonadIO m
  => Generic a
  => Show a
  => GVertex (Rep a)
  => GLenum -- ^ target
  -> [a] -- ^ vertex attribs as Haskell data
  -> GLenum -- ^ usage
  -> m ()
glBufferRecord target vs usage = do
  let attrSpec = gvertexSpec @(Rep a)
  let stride = fromIntegral $ sum $ fmap asByteSize attrSpec
  let vboSize = stride * length vs
  liftIO $ traceIO $ unwords
    [ "glBufferRecord"
    , "vs =", show vs
    , "attrSpec =", show attrSpec
    , "stride =", show stride
    , "vboSize =", show vboSize
    ]
  liftIO $ allocaBytes vboSize $ \vboBytes -> do
    for_ ([0, stride..] `zip` vs) $ \(off, v) -> do
      let p = vboBytes `plusPtr` off
      gpokeVertex (vboBytes `plusPtr` off) (from v)
      liftIO $ traceIO $ unwords ["gpokeVertex:", show off, show v]
      v' :: a <- to <$> gpeekVertex p
      liftIO $ traceIO $ unwords ["gpeekVertex:", show v']
    glBufferData target (fromIntegral vboSize) vboBytes usage

data GL_VertexAttribPointerArgs = GL_VertexAttribPointerArgs
  { glvapIndex :: GLuint
  , glvapSize :: GLint
  , glvapType :: GLenum
  , glvapNormalized :: GLboolean
  , glvapStride :: GLsizei
  , glvapPointer :: Ptr ()
  } deriving stock (Eq, Ord, Show)

glEnableVertexAttribRecord
  :: forall a m
   . MonadIO m
  => Generic a
  => GVertex (Rep a)
  => m ()
glEnableVertexAttribRecord = do
  for_ ([0..] `zip` gvertexSpec @(Rep a)) $ \(i, _) ->
    glEnableVertexAttribArray i

glVertexAttribRecord
  :: forall a m
   . MonadIO m
  => Generic a
  => GVertex (Rep a)
  => m ()
glVertexAttribRecord =
  for_ (mk'glVertexAttribPointerArgs @a) $ \x@GL_VertexAttribPointerArgs{..} -> do
    liftIO $ traceIO $ unwords ["attribptr =", show x] 
    glVertexAttribPointer glvapIndex glvapSize glvapType glvapNormalized glvapStride glvapPointer
    glEnableVertexAttribArray glvapIndex

mk'glVertexAttribPointerArgs
  :: forall a
   . Generic a
  => GVertex (Rep a)
  => [GL_VertexAttribPointerArgs]
mk'glVertexAttribPointerArgs = do
  let vspec = gvertexSpec @(Rep a)
  let stride = sum $ fmap asByteSize vspec
  let offsets = fromIntegral <$> scanl (\off AttribSpec{..} -> off + asByteSize) 0 vspec
  flip fmap (zip3 [0..] offsets vspec) $ \(i, offset, AttribSpec{..}) -> do
    let normalized = if asNormalized then GL_TRUE else GL_FALSE
    GL_VertexAttribPointerArgs
      { glvapIndex = i
      , glvapSize = fromIntegral asSize
      , glvapType = asType
      , glvapNormalized = normalized
      , glvapStride = stride
      , glvapPointer = nullPtr `plusPtr` offset
      }

class GVertex a where
  gvertexSpec :: [AttribSpec]
  gpokeVertex :: Ptr () -> a x -> IO ()
  gpeekVertex :: Ptr () -> IO (a x)

instance (GVertex x, GVertex y) => GVertex (x :*: y) where
  gvertexSpec = gvertexSpec @x ++ gvertexSpec @y
  gpokeVertex ptr (x :*: y) = do
    gpokeVertex ptr x
    let xoff = fromIntegral $ sum $ fmap asByteSize $ gvertexSpec @x
    gpokeVertex (ptr `plusPtr` xoff) y
  gpeekVertex ptr =
    let xoff = fromIntegral $ sum $ fmap asByteSize $ gvertexSpec @x
     in (:*:)
    <$> gpeekVertex ptr
    <*> gpeekVertex (ptr `plusPtr` xoff)

instance GVertex a => GVertex (M1 _x _y a) where
  gvertexSpec = gvertexSpec @a
  gpokeVertex ptr (M1 a) = gpokeVertex ptr a
  gpeekVertex ptr = M1 <$> gpeekVertex ptr

instance (Finite v, KnownNat (Size v), Storable (v a), IsAttribType a) => GVertex (K1 _x (v a)) where
  gvertexSpec =
    let vs = natVal (Proxy @(Size v))
    in pure AttribSpec
       { asSize = fromIntegral vs
       , asType = attribTypeType @a
       , asNormalized = attribTypeNormalized @a
       , asByteSize = fromIntegral $ sizeOf (undefined :: v a)
       }

  gpokeVertex ptr (K1 va) = poke (castPtr ptr) va
  gpeekVertex ptr = K1 <$> peek (castPtr ptr)

class IsAttribType a where
  attribTypeNormalized :: Bool
  attribTypeType :: GLenum

instance IsAttribType GLfloat where
  attribTypeNormalized = False
  attribTypeType = GL_FLOAT
