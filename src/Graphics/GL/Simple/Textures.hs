module Graphics.GL.Simple.Textures where

import UnliftIO.Foreign
import Codec.Picture
import Control.Monad.IO.Unlift
import qualified Data.Vector.Storable as VS

import Graphics.GL.Types
import Graphics.GL.Core33

import Debug.Trace

glLoadPictureFile :: MonadUnliftIO m => FilePath -> m GLuint
glLoadPictureFile path = liftIO (readImage path) >>= glHandleJuicyResult path >>= glTexDynamicImage

glHandleJuicyResult :: Applicative m => FilePath -> Either String DynamicImage -> m DynamicImage
glHandleJuicyResult path (Left err) = error $ unwords ["Error loading texture image", path, "..", err]
glHandleJuicyResult _ (Right di) = pure di

glWithImagePtr
  :: MonadUnliftIO m
  => Num n
  => Storable (PixelBaseComponent p)
  => Image p
  -> (n -> n -> Ptr () -> m a)
  -> m a
glWithImagePtr img k = do
  let iw = fromIntegral $ imageWidth img
  let ih = fromIntegral $ imageWidth img
  runInIO <- askRunInIO
  liftIO $ VS.unsafeWith (imageData img) (runInIO . k iw ih . castPtr)

glTexDynamicImage :: MonadUnliftIO m => DynamicImage -> m GLuint
glTexDynamicImage di = alloca $ \texP -> do
  glGenTextures 1 texP
  tex <- liftIO $ peek texP
  glBindTexture GL_TEXTURE_2D tex
  -- wrapping and filtering params would go here
  let img2tex ifmt fmt img =
        glWithImagePtr img $ \iw ih bs -> liftIO $ do
          t <- glTexImage2D GL_TEXTURE_2D 0 ifmt iw ih 0 fmt GL_UNSIGNED_BYTE bs
          glGetError >>= \err -> traceIO $ "glTexImage2D error: " ++ show err
          pure t

  let err ty img = do
        let msg = "Unhandled JuicyPixels type: " ++ ty
        liftIO $ traceIO msg
        img2tex GL_RGBA8 GL_RGBA img
        liftIO $ traceIO "img2tex complete"
        --error msg

  case di of
    ImageRGB8 img -> img2tex GL_RGB8 GL_RGB img
    ImageRGBA8 img -> img2tex GL_RGBA8 GL_RGBA img
    ImageY8 _ -> err "ImageY8" $ convertRGBA8 di
    ImageY16 _ -> err "ImageY16" $ convertRGBA8 di
    ImageY32 _ -> err "ImageY32" $ convertRGBA8 di
    ImageYF _ -> err "ImageYF" $ convertRGBA8 di
    ImageYA8 _ -> err "Image YA8" $ convertRGBA8 di
    ImageYA16 _ -> err "ImageY16" $ convertRGBA8 di
    ImageRGB16 _ -> err "ImageRGB16" $ convertRGBA8 di
    ImageRGBF _ -> err "ImageRGBF" $ convertRGBA8 di
    ImageRGBA16 _ -> err "ImageRGBA16" $ convertRGBA8 di
    ImageYCbCr8 _ -> err "ImageYCbCr8" $ convertRGBA8 di
    ImageCMYK8 _ -> err "ImageCMYK8" $ convertRGBA8 di
    ImageCMYK16 _ -> err "ImageCMYK16" $ convertRGBA8 di

  glGenerateMipmap GL_TEXTURE_2D
  glBindTexture GL_TEXTURE_2D 0
  pure tex
