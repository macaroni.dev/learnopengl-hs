with import <nixpkgs> {};
haskellPackages.shellFor {
  withHoogle = true;
  packages = p: [(import ./default.nix)];
  buildInputs = with haskellPackages; [ cabal-fmt renderdoc ];
}
