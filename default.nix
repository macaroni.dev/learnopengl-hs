with import <nixpkgs> {};
(haskellPackages.override {
  overrides = self: super: {
    jrec = haskell.lib.doJailbreak(self.callCabal2nix "jrec" (fetchFromGitHub {
      owner = "juspay";
      repo = "jrec";
      rev = "3be4f9c86a59c40e5c83a7d4497f7b15cecafc94";
      sha256 = "sha256-SkNHvXQWbk9Jl+Yjv1pkliwjOjEDjU88uhd2RYf2skY=";
    }) {});
  };
}).callCabal2nix "macaroni-learnopengl" ./. {}

